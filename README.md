# JB Hired Code Challenges

This repository houses JB Hired's interviewing code challenges. These are small hands-on
projects that are very relevant to the tasks you will be working on at JB Hired.

### Ground Rules

* We prefer well-thought-out solutions over the quick-and-dirty kind. So take your time,
  if you need it. A rushed job is usually matched by a swift rejection.
* Submission is done via a [git format-patch](https://git-scm.com/docs/git-format-patch). Attach
  your patches in an e-mail and send it to [yassine@jbhired.com](mailto:yassine@jbhired.com).

### Challenges

Pick the one that most suites the position you wish to apply.

**JB Hired**

* Backend / Python Django - [python-django](https://gitlab.com/jbengine/challenges/tree/master/python-django)
* Frontend / React - [react](https://gitlab.com/jbengine/challenges/tree/master/react)

### Free Style Challenge

If you feel that none of the challenges are sufficient to demonstrate your coding ability,
you can also choose to submit any project of your own choosing that you are most proud of.
