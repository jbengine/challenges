Create a front-end application allowing a user to interact with a Kanban board.

Keep track of the time you spend on each part of this project and send it with your submission to [yassine@jbhired.com](mailto:yassine@jbhired.com).

# Step 1

The first step focuses only on the frontend side. 

- Research what is a Kanban board and how it can be used for recruitment
- Get inspired by Pipedrive, Trello, Jira [or this example](https://alexandre-paroissien.github.io/kanban/index.html) (feel free to ignore it and do your own, this is just to give you a better idea of what's expected).
- Develop or implement a Kanban board in React following the previous example (drag and drop).
- (Bonus) Show off your CSS skills by making it look good.

# Step 2

This second step focuses on the communication with API.

- Imagine that in the backend database, each person object has a field "status" which can be "interview", "technical_test", or "hired"
- And imagine that the there is a REST API providing CRUD operations for person object
- Explain how you would connect the frontend to the API to fetch the data and modify the data presented in your Kanban board

# Guidelines

- Send us the project and your comments as a git patch or repository, or a hosted live demo
